# Individual Project 2 [![pipeline status](https://gitlab.com/RivocX/individual-project-2/badges/master/pipeline.svg)](https://gitlab.com/RivocX/individual-project-2/-/commits/master)
> Puyang(Rivoc) Xu (px16)

This project is a simple REST API/web service in Rust. Also add Dockerfile to containerize service and `.gitlab-ci.yml` for CI/CD pipeline. Just like Mini Project 4, this project uses Actix-web. The main function is to calculate the square and the square root of the number entered.

Demo Video: [Video →](https://gitlab.com/RivocX/individual-project-2/-/blob/master/media/demo.mov?ref_type=heads)

## Detailed steps
### Preparation
The installation of `Cargo` and `Docker` is needed for this project.
Create a new cargo project:
```
cargo new rust_microservice
```

Modify dependencies in `Cargo.toml`:
```
serde_json = "1"
lambda_http = "0.8.3"
lambda_runtime = "0.9.1"
serde = "1.0.136"
tokio = { version = "1", features = ["macros"] }
tracing = { version = "0.1", features = ["log"] }
tracing-subscriber = { version = "0.3", default-features = false, features = ["env-filter", "fmt"] }
actix-web = "4.5.1"
```


### Rust Microservice
Write `main.rs`. Here the simple rust microservice is to calculate the square and the square root of the entered number.
Test locally:
```
cargo run
```
The returned JSON message will be displayed on the website.


### Docker
Write Dockerfile as following:
```
# Use the official Rust 1.75 image as the base image for the builder stage
FROM rust:1.75 AS builder

# Set the working directory inside the container
WORKDIR /usr/src/ip2_rms

# Change the user to root for copying files (optional, depending on your needs)
USER root

# Copy the entire content of the local directory into the container's working directory
COPY . .

# Build the project using Cargo with the --release flag for optimized code
RUN cargo build --release

# Expose port 8080 to the outside world (used by Actix-Web)
EXPOSE 8080

# Define the default command to run when the container starts, using 'cargo run'
CMD cargo run
```

Then build Docker:
```
sudo docker build -t ip2_rms .
```

After building, run Docker:
```
sudo docker run -p 8080:8080 ip2_rms
```

You can access the website by this url: http://localhost:8080/
An example of this microservice can be access by this url: http://localhost:8080/calculate?number=5
You will get the square and the square root of 5.

### CI/CD Pipline
Write `.gitlab-ci.yml` as following:
```
# Define stages for the pipeline
stages:
  - build

# Define variables that can be used across jobs
variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2

build:
  stage: build
  image: docker:stable  # Use the Docker image for running Docker commands
  services:
    - docker:dind  # Enable Docker-in-Docker service for building Docker images
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"  # Log in to the GitLab Container Registry
  script:
    - docker build -t rust-microservice .
    - docker run -d -p 8080:8080 rust-microservice
    - docker ps -a
  after_script:
    - echo "Rust microservice is now running on port 8080"
```

`.gitlab-ci.yml` is the GitLab CI/CD configuration file that defines the CI/CD process for the project. Push the project to GitLab, then you will see the pipline is successfully enabled.

### Demo Video
Demo Video: [Video →](https://gitlab.com/RivocX/individual-project-2/-/blob/master/media/demo.mov?ref_type=heads)


## Results & Screenshots

### Docker
#### Docker build
![](./media/docker_build.png)
#### Docker run
![](./media/docker_run.png)
#### Deployed project on Docker app
![](./media/docker_app.png)

### Results
#### Default page of website
![](./media/web1.png)
#### Response page of website(number = 5)
![](./media/web2.png)

### CI/CD Pipline
![](./media/pipline.png)