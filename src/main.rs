use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use serde::Serialize;
use std::collections::HashMap;

#[derive(Serialize)]
struct CalculationResult {
    message: String, // Use a String to hold the custom message
}

#[get("/")]
async fn index() -> impl Responder {
    "To calculate the square and the square root, please use the following link: http://localhost:8080/calculate?number=<numbertocalculate>\nEg. http://localhost:8080/calculate?number=5"
}

#[get("/calculate")]
async fn calculate(web::Query(info): web::Query<HashMap<String, String>>) -> impl Responder {
    match info.get("number") {
        Some(num_str) => {
            match num_str.parse::<i32>() {
                Ok(number) => {
                    let square = number * number;
                    let sqrt = (number as f64).sqrt(); // Convert to f64 to calculate the square root
                    let message = format!("Calculation successful! The square of the number {} is {}, and the square root is {:.2}", number, square, sqrt);
                    let response = CalculationResult { message };
                    HttpResponse::Ok().json(response)
                }
                Err(_) => HttpResponse::BadRequest().body("Invalid number provided"),
            }
        }
        None => HttpResponse::BadRequest().body("Missing number query parameter"),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(index)
            .service(calculate) // Update service name
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
